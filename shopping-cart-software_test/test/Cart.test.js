const Cart = require('../src/Cart.js');
const expect = require('chai').expect;

describe('Cart', () => {
    it('should initialize as empty', () => {
        const cart = new Cart()

        expect(cart.items).to.deep.equal([])
        expect(cart.totalPrice).to.be.equal(0)
    });
	
	it('should have 1 item', () => {        
		const cart1 = new Cart()
        cart1.addItem('Handbag', 2) 
        expect(cart1.items).to.deep.equal['Handbag - 2']
    });
	
	it('should have 2 items',() => {        
		const cart2 = new Cart()
        cart2.addItem('Handbag', 2) 
        cart2.addItem('Watch', 3) 
		expect(cart2.items).to.deep.equal[{"product": "Handbag", "qty": 2}, {"product": "Watch", "qty": 3}]
		
    });
})
