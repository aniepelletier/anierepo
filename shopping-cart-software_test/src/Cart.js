module.exports = class Cart {
  constructor() {
    this.items = [];
  }

  get totalPrice () {
    return this.items.reduce(function(total, item)  {
      return total + item.product.price * item.qty;
    }, 0.00);
  }

  addItem(item, quantity) {
    this.items.push({product: item, qty: quantity});
  }
  
  itemQuantities () {
	return this.items.forEach(function(item){			
	  item.product.onSale = 'x';
	  return item.product.name + " - " + item.product.onSale  + item.qty;})
  }

  itemizedList () {
	return this.items.forEach(function(item){ 
	  item.product.onSale = 'x';
	  return item.product.name + " " + item.product.onSale + item.qty + " - $" + totalPrice();})
  }
  
  onSaleItems () {
	return this.items.forEach(function(item){			
	 while (item.product.onSale == 'x'){
	 return item.product.name + " - " + item.product.onSale  + item.qty + " $" + totalPrice;}
		})
	}
}